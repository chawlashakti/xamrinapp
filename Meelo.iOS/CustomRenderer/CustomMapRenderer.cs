﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;
using Xamarin.Forms.Maps.iOS;
using MapKit;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using CoreLocation;
using Meelo;
using Meelo.iOS;
using CoreGraphics;
using Xamarin.Forms.Maps;
using System.ComponentModel;
using Plugin.Geolocator;
using System.Collections.ObjectModel;
using Meelo.CustomControls;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace Meelo.iOS
{
    public class CustomMapRenderer : MapRenderer
    {
        UIView customPinView;
        ObservableCollection<CustomPin> customPins;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                if (nativeMap != null)
                {
                    nativeMap.RemoveAnnotations(nativeMap.Annotations);
                    nativeMap.GetViewForAnnotation = null;
                    nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
                }
            }

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                var nativeMap = Control as MKMapView;
                customPins = formsMap.CustomPins;

                nativeMap.GetViewForAnnotation = GetViewForAnnotation;
                nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
            }
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var map = (CustomMap)sender;
            customPins = map.CustomPins;

        }
        MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            if (annotation is MKUserLocation)
                return null;

            var customPin = GetCustomPin(annotation as MKPointAnnotation);
            if (customPin == null)
            {
                customPin = new CustomPin();
                customPin.Id = "Pinned";
            }
            annotationView = mapView.DequeueReusableAnnotation(customPin.Id.ToString());
            if (annotation != null || annotationView == null)
            {
                    if (customPin.Label == "User's Location")
                    {
                        annotationView = new MKAnnotationView(annotation, customPin.Id.ToString())
                        {
                            Image = UIImage.FromFile("UserMapIcon.png"),
                        };
                        annotationView.CanShowCallout = true;
                    }
                    else
                    {
                        annotationView = new MKAnnotationView(annotation, customPin.Id.ToString())
                        {
                            Image = UIImage.FromFile("map_pin.png"),
                        };
                        //for (int i = 0; i < App.Locator.SearchWorkspaceViewModelInstance.workspaceData.value.Count; i++)
                        //{
                            //if (annotation.Coordinate.Latitude == App.Locator.SearchWorkspaceViewModelInstance.workspaceData.value[i].Lat && annotation.Coordinate.Longitude == App.Locator.SearchWorkspaceViewModelInstance.workspaceData.value[i].Long)
                            //{
                                annotationView.Tag = Convert.ToInt32(customPin.BuildingId);

                                UIView v = new UIView();
                                UILabel l = new UILabel();
                                v.BackgroundColor = UIColor.FromRGB(2, 119, 189);
                                l.BackgroundColor = UIColor.FromRGB(2, 119, 189);
                                v.Frame = new CGRect(annotationView.Frame.Size.Width - 5, 0, 20, 20);
                                l.Frame = new CGRect(2, 5, 14, 8);
                                l.Text = customPin.WorkspaceCount.ToString();
                                l.TextAlignment = UITextAlignment.Center;
                                l.Font = l.Font.WithSize(10f);
                                l.TextColor = UIColor.White;
                                v.Add(l);
                                v.Layer.CornerRadius = 10;

                                annotationView.Add(v);
                            //}
                        //}

                        var tapRecogniser = new UITapGestureRecognizer(this, new ObjCRuntime.Selector("MapTapSelector:"));
                        annotationView.AddGestureRecognizer(tapRecogniser);


                        //annotationView.Annotation.Coordinate
                    }
            }
            return annotationView;
        }

        [Export("MapTapSelector:")]
        protected void OnMapTapped(UIGestureRecognizer sender)
        {
            try
            {
                UIView obj = sender.View;
                for (int i = 0; i < App.Locator.MapPageViewModelInstance.WorkspaceRecord.Count; i++)
                {
                    if (obj.Tag.ToString() == App.Locator.MapPageViewModelInstance.WorkspaceRecord[i].BuildingId)
                    {
                        App.Locator.MapPageViewModelInstance.LoadListOnPinTap(App.Locator.MapPageViewModelInstance.WorkspaceRecord[i].Lat, App.Locator.MapPageViewModelInstance.WorkspaceRecord[i].Long, App.Locator.MapPageViewModelInstance.WorkspaceRecord[i].BuildingId);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            
        }

        void OnDidSelectAnnotationView(object sender, MKAnnotationViewEventArgs e)
        {

        }
        CustomPin GetCustomPin(MKPointAnnotation annotation)
        {
            var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
            foreach (var pin in customPins)
            {
                if (pin.Position == position)
                {
                    return pin;
                }
            }
            return null;
        }
        //CustomPin GetCustomPin(MKPointAnnotation annotation)
        //{
        //    var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
        //    return customPins.FirstOrDefault(pin => pin.Pin.Position == position);
        //}
    }
}