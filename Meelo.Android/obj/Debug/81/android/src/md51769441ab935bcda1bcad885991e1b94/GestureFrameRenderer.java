package md51769441ab935bcda1bcad885991e1b94;


public class GestureFrameRenderer
	extends md51558244f76c53b6aeda52c8a337f2c37.VisualElementRenderer_1
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Meelo.Droid.CustomRenderer.GestureFrameRenderer, Meelo.Android", GestureFrameRenderer.class, __md_methods);
	}


	public GestureFrameRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == GestureFrameRenderer.class)
			mono.android.TypeManager.Activate ("Meelo.Droid.CustomRenderer.GestureFrameRenderer, Meelo.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public GestureFrameRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == GestureFrameRenderer.class)
			mono.android.TypeManager.Activate ("Meelo.Droid.CustomRenderer.GestureFrameRenderer, Meelo.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}


	public GestureFrameRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == GestureFrameRenderer.class)
			mono.android.TypeManager.Activate ("Meelo.Droid.CustomRenderer.GestureFrameRenderer, Meelo.Android", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
