﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Meelo.Droid.DependencyService;
using Meelo.Interfaces;
using Meelo.Services;
using Xamarin.Forms;
[assembly: Dependency(typeof(GoogleConfig))]
namespace Meelo.Droid.DependencyService
{
    public class GoogleConfig : IGoogleAuthenticationConfiguration
    {
        public Services.GoogleConfiguration GetConfiguration()
        {
            return new Services.GoogleConfiguration
            {
                ClientId = "27210327045-2h6ntvdrvfc4pi4idsupqma9mpc4qnqh.apps.googleusercontent.com",
                //ClientId = " 1027147958306-i4kamcof9ondubs30tm9odqcep3n8b06.apps.googleusercontent.com",
                RedirectUrl = "com.meeloinc.meelo:/oauth2redirect"
            };
        }
    }
}