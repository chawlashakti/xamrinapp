﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Meelo.Droid.CustomRenderer
{
    public class CustomGestureListener : GestureDetector.SimpleOnGestureListener
    {
        private static int SWIPE_THRESHOLD = 150;
        private static int SWIPE_VELOCITY_THRESHOLD = 150;

        private static int SCROLL_SWIPE_THRESHOLD = 5;
        private static int SCROLL_SWIPE_VELOCITY_THRESHOLD = 5;

        public event EventHandler OnSwipeDown;
        public event EventHandler OnSwipeTop;
        public event EventHandler OnSwipeLeft;
        public event EventHandler OnSwipeRight;
        public event EventHandler OnTapped;

        public override bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            Console.WriteLine("OnFling");

            float diffY = e2.GetY() - e1.GetY();
            float diffX = e2.GetX() - e1.GetX();

            if (Math.Abs(diffX) > Math.Abs(diffY))
            {
                if (Math.Abs(diffX) > SWIPE_THRESHOLD && Math.Abs(velocityX) > SWIPE_VELOCITY_THRESHOLD)
                {
                    if (diffX > 0)
                    {
                        if (OnSwipeRight != null)
                            OnSwipeRight(this, null);
                    }
                    else if (diffX < 0)
                    {
                        if (OnSwipeLeft != null)
                            OnSwipeLeft(this, null);
                    }
                    else
                    {
                        if (OnTapped != null)
                        {
                            OnTapped(this, null);
                        }
                    }
                }
            }
            else
            {
                if (OnTapped != null)
                {
                    OnTapped(this, null);
                }
            }
            return base.OnFling(e1, e2, velocityX, velocityY);
        }

        public override bool OnScroll(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            float diffY = e2.GetY() - e1.GetY();
            float diffX = e2.GetX() - e1.GetX();

            if (Math.Abs(diffX) > Math.Abs(diffY))
            {
                if (Math.Abs(diffX) > SCROLL_SWIPE_THRESHOLD && Math.Abs(velocityX) > SCROLL_SWIPE_VELOCITY_THRESHOLD)
                {
                    if (diffX > 0)
                    {
                        if (OnSwipeRight != null)
                            OnSwipeRight(this, null);
                    }
                    else
                    {
                        if (OnSwipeLeft != null)
                            OnSwipeLeft(this, null);
                    }
                }
            }
            else
            {
                if (OnTapped != null)
                {
                    OnTapped(this, null);
                }
            }
            return base.OnScroll(e1, e2, velocityX, velocityY);
        }
        public override Boolean OnSingleTapUp(MotionEvent e)
        {
            if (OnTapped != null)
            {
                OnTapped(this, null);
            }
            return true;
        }
    }
}