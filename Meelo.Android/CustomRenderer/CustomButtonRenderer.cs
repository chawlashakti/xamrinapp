﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Meelo;
using Meelo.Droid;
using Meelo.Droid.CustomRenderer;
using Meelo.CustomControls;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace Meelo.Droid.CustomRenderer
{
    public class CustomButtonRenderer :ButtonRenderer
    {
        public CustomButtonRenderer(Context context):base(context)
        {

        }
        
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                //Control.SetBackgroundColor(Android.Graphics.Color.Red);
                //Control.SetBackgroundResource(Resource.Drawable.CustomButtonBackground);
            }
        }
    }
}