﻿using Microsoft.Identity.Client;
using System;
namespace Meelo.Helper
{
    public static class Constants
    {
        public enum LoginPlatform {
            Office365=1,
            Google=2,
        };
        public enum PageView
        {
            Map = 1,
            List = 2,
            Search = 3
        }
        public const string GoogleScope = "profile";
        public static string ApiBaseUrl = "http://meeloapibasic.azurewebsites.net";
        //Pages
        public const string LoginOptions = "LoginOptionsPage";
        public const string MainTabbedPage = "MainTabbedPage";
        public const string MapPage = "MapPage";
        public const string ListViewPage = "ListViewPage";
        public const string AppsPage = "AppsPage";
        public const string MeetingPage = "MeetingPage";
        public const string MorePage = "MorePage";
        public const string InvitePage = "InvitePage";

        public static string AppName = "OAuthNativeFlow";

        // OAuth
        // For Google login, configure at https://console.developers.google.com/
        public static string iOSClientId = "442063281199-ovhb71j1661i160lsvg777qkh46njj4t.apps.googleusercontent.com";
        public static string AndroidClientId = "380347857069-94h9ipr7dfj8htqmhqnutfhkg5t2n147.apps.googleusercontent.com";
        // Shakti Android client id
      //  public const string AndroidClientId = "1027147958306-i4kamcof9ondubs30tm9odqcep3n8b06.apps.googleusercontent.com";

        // These values do not need changing
        public static string Scope = "https://www.googleapis.com/auth/userinfo.email";
        public static string AuthorizeUrl = "https://accounts.google.com/o/oauth2/auth";
        public static string AccessTokenUrl = "https://accounts.google.com/o/oauth2/token";
        public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";

        // Set these to reversed iOS/Android client ids, with :/oauth2redirect appended
        public static string iOSRedirectUrl = "com.googleusercontent.apps.442063281199-ovhb71j1661i160lsvg777qkh46njj4t:/oauth2redirect";
        public static string AndroidRedirectUrl = "https://accounts.google.com/o/oauth2/auth";

        public static string facebookClientId = "137004533674208";
        public static string FacebookAuthorizeUrl = "https://m.facebook.com/dialog/oauth/";
        public static string iOSFacebookRedirectUrl = "fb137004533674208://authorize";

        public static string FacebookUserInfoUrl = "https://graph.facebook.com/me?fields=name,picture,cover,birthday";


        //Office 365 
       
        public static string[] Scopes = {
                        "https://graph.microsoft.com/Mail.Send",
                        "https://graph.microsoft.com/Calendars.ReadWrite",
                        "https://graph.microsoft.com/Mail.ReadWrite",
                        "https://graph.microsoft.com/Files.Read.All"
                    };
        public static string AuthorityLink= "https://login.windows.net/common";
        public static string ClientAppId = "7a06dbb7-4bbd-4a3d-b5ed-e1ee0801eee4";
        public static string GraphURILink = "https://graph.windows.net";
        public static string ReturnURILink = "msal7a06dbb7-4bbd-4a3d-b5ed-e1ee0801eee4";
    }
}
