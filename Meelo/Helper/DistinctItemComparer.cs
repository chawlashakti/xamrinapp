﻿using Meelo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Helper
{
    public class DistinctItemComparer : IEqualityComparer<WorkspaceModel>
    {

        public bool Equals(WorkspaceModel x, WorkspaceModel y)
        {
            return x.BuildingId == y.BuildingId;
        }

        public int GetHashCode(WorkspaceModel obj)
        {
            return obj.BuildingId.GetHashCode();
        }
    }
}
