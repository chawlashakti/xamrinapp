﻿using Meelo.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meelo.Interfaces
{
   public interface IGoogleAuthenticationConfiguration
    {
        GoogleConfiguration GetConfiguration();
    }
}
