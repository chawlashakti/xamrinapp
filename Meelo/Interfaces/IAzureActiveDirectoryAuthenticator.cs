﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Interfaces
{
    public interface IAzureActiveDirectoryAuthenticator
    {
        Task<string> Authenticate(string authority, string resource, string clientId, string returnUri);
    }
}
