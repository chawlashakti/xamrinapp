﻿using Meelo.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.CustomControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchViewControl : StackLayout
	{
        public ListView SuggestionsListView { get; private set; }
        //public CustomEntry SearchEntry { get; private set; }
        public IEnumerable OriginSuggestions { get; private set; }
        //public ScrollView SuggestionWrapper { get; private set; }
        //public Grid EntryContainer { get; private set; }
        //public ImageButton ClearIconImage { get; private set; }
        public int TotalNumberOfTypings { get; private set; }
        public IEnumerable Suggestions
        {
            get { return (IEnumerable)GetValue(SuggestionsProperty); }
            set { SetValue(SuggestionsProperty, value); }
        }

        public string SearchText
        {
            get { return (string)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }
        //public AzureSearchModel SelectedSearchText
        //{
        //    get { return (AzureSearchModel)GetValue(SelectedSearchTextProperty); }
        //    set { SetValue(SelectedSearchTextProperty, value); }
        //}
        
        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public int MaximumVisibleSuggestionItems
        {
            get { return (int)GetValue(MaximumVisibleSuggestionItemsProperty); }
            set { SetValue(MaximumVisibleSuggestionItemsProperty, value); }
        }

        public DataTemplate SuggestionItemTemplate
        {
            get { return (DataTemplate)GetValue(SuggestionItemTemplateProperty); }
            set { SetValue(SuggestionItemTemplateProperty, value); }
        }

        public string DisplayPropertyName
        {
            get { return (string)GetValue(DisplayPropertyNameProperty); }
            set { SetValue(DisplayPropertyNameProperty, value); }
        }
        public static readonly BindableProperty SuggestionsProperty = BindableProperty.Create(nameof(Suggestions), typeof(IEnumerable), typeof(SearchViewControl), null, BindingMode.OneWay, null, OnSuggestionsChanged);
        public static readonly BindableProperty SearchTextProperty = BindableProperty.Create(nameof(SearchText), typeof(string), typeof(SearchViewControl), null, BindingMode.TwoWay, null, OnSearchTextChanged);
        //public static readonly BindableProperty SelectedSearchTextProperty = BindableProperty.Create(nameof(SelectedSearchText), typeof(AzureSearchModel), typeof(SearchViewControl), null, BindingMode.TwoWay, null);
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(SearchViewControl), null, BindingMode.OneWay, null, OnPlaceholderChanged);
        public static readonly BindableProperty MaximumVisibleSuggestionItemsProperty = BindableProperty.Create(nameof(MaximumVisibleSuggestionItems), typeof(int), typeof(SearchViewControl), 4);
        public static readonly BindableProperty SuggestionItemTemplateProperty = BindableProperty.Create(nameof(SuggestionItemTemplate), typeof(DataTemplate), typeof(SearchViewControl), null, BindingMode.OneWay, null, OnSuggestionItemTemplateChanged);
        public static readonly BindableProperty DisplayPropertyNameProperty = BindableProperty.Create(nameof(DisplayPropertyName), typeof(string), typeof(SearchViewControl));
        private static void OnSuggestionsChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as SearchViewControl;

            var suggestions = (IEnumerable)newValue;
            autoCompleteView.OriginSuggestions = suggestions;

            suggestions = autoCompleteView.FilterSuggestions(suggestions, autoCompleteView.SearchText);
            autoCompleteView.SuggestionsListView.ItemsSource = suggestions;
        }
        private static void OnSearchTextChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as SearchViewControl;
            if (autoCompleteView.SearchText == "")
                autoCompleteView.SearchEntry.Text = "";

                var suggestions = autoCompleteView.OriginSuggestions;
            if (newValue != null && newValue.ToString().Length > 0)
            {
                suggestions = autoCompleteView.FilterSuggestions(suggestions, autoCompleteView.SearchText);
                // assign when initializing with data
                if (autoCompleteView.SearchEntry.Text != autoCompleteView.SearchText)
                {
                    autoCompleteView.SearchEntry.Text = autoCompleteView.SearchText;
                }
            }
            autoCompleteView.SuggestionsListView.ItemsSource = suggestions;

            
        }

        private static void OnSuggestionItemTemplateChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as SearchViewControl;

            if (autoCompleteView.SuggestionsListView != null)
            {
                autoCompleteView.SuggestionsListView.ItemTemplate = autoCompleteView.SuggestionItemTemplate;
            }
        }

        public IEnumerable FilterSuggestions(IEnumerable suggestions, string keyword)
        {
            if (string.IsNullOrEmpty(keyword) || suggestions == null) return suggestions;

            return suggestions;
        }

        private static void OnPlaceholderChanged(object bindable, object oldValue, object newValue)
        {
            var autoCompleteView = bindable as AutoCompleteView;
            autoCompleteView.SearchEntry.Placeholder = newValue?.ToString();
        }
        private void SearchEntry_TextChanged(object sender, TextChangedEventArgs e)
        {
            TotalNumberOfTypings++;
            Device.StartTimer(TimeSpan.FromMilliseconds(700), () => {
                TotalNumberOfTypings--;
                if (TotalNumberOfTypings == 0)
                {
                    SearchText = e.NewTextValue;
                }
                return false;
            });
        }

        private void SearchEntry_Focused(object sender, FocusEventArgs e)
        {
            ClearIconImage.IsVisible = true;
            if (this.OnFocusChanged != null)
            {
                this.OnFocusChanged(sender, e);
            }
        }

        private void SearchEntry_Unfocused(object sender, FocusEventArgs e)
        {
            //Container.HeightRequest = 50;
            //Container.ForceLayout();
            //ClearIconImage.IsVisible = false;
        }


        public static readonly BindableProperty ClearCommandProperty =
        BindableProperty.Create(nameof(ClearCommand), typeof(ICommand), typeof(SearchViewControl), null);

        public ICommand ClearCommand
        {
            get { return (ICommand)GetValue(ClearCommandProperty); }
            set { SetValue(ClearCommandProperty, value); }
        }
        public event EventHandler<EventArgs> OnFocusChanged;
        public event EventHandler<SelectedItemChangedEventArgs> ItemSelected;
        public static readonly BindableProperty CancelCommandProperty =
       BindableProperty.Create(nameof(CancelCommand), typeof(ICommand), typeof(SearchViewControl), null);

        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }
        public static readonly BindableProperty SearchCommandProperty =
      BindableProperty.Create(nameof(SearchCommand), typeof(ICommand), typeof(SearchViewControl), null);

        public ICommand SearchCommand
        {
            get { return (ICommand)GetValue(SearchCommandProperty); }
            set { SetValue(SearchCommandProperty, value); }
        }
        public static void Execute(ICommand command)
        {
            if (command == null) return;
            if (command.CanExecute(null))
            {
                command.Execute(null);
            }
        }
        public static readonly BindableProperty SearchListViewProperty = BindableProperty.Create(nameof(SearchListView), typeof(bool), typeof(SearchViewControl), false,propertyChanged: SearchListViewVisibilityChanged);
        public bool SearchListView
        {
            get { return (bool)GetValue(SearchListViewProperty); }
            set { SetValue(SearchListViewProperty, value); }
        }
        static void SearchListViewVisibilityChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (SearchViewControl)bindable;
            control.searchListView.IsVisible =Convert.ToBoolean(newValue);
        }
        public static readonly BindableProperty SearchHeaderProperty = BindableProperty.Create(nameof(SearchHeader), typeof(bool), typeof(SearchViewControl), false, propertyChanged: SearchHeaderVisibilityChanged);
        public bool SearchHeader
        {
            get { return (bool)GetValue(SearchHeaderProperty); }
            set { SetValue(SearchHeaderProperty, value); }
        }
        static void SearchHeaderVisibilityChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (SearchViewControl)bindable;
            control.SearchControlHeader.IsVisible = Convert.ToBoolean(newValue);
        }
        public static readonly BindableProperty ClearIconIsVisibleProperty = BindableProperty.Create(nameof(ClearIconIsVisible), typeof(bool), typeof(SearchViewControl), false,propertyChanged: ClearSearchIconVisibilityChanged);
        public bool ClearIconIsVisible
        {
            get { return (bool)GetValue(ClearIconIsVisibleProperty); }
            set { SetValue(ClearIconIsVisibleProperty, value); }
        }
        static void ClearSearchIconVisibilityChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (SearchViewControl)bindable;
            control.ClearSearchIcon.IsVisible = Convert.ToBoolean(newValue);
        }
        public SearchViewControl ()
		{
			InitializeComponent ();
            searchListView.SetBinding(IsVisibleProperty, new Binding(path: "SearchListView", source: this));
            SearchControlHeader.SetBinding(IsVisibleProperty, new Binding(path: "SearchHeader", source: this));
            ClearSearchIcon.SetBinding(ImageButton.IsVisibleProperty, new Binding(path: "ClearIconIsVisible", source: this));
            ClearIconImage.Command = new Command(_ => Execute(ClearCommand));
            CancelSearch.Command = new Command(_ => Execute(CancelCommand));
            SubmitSearch.Command = new Command(_ => Execute(SearchCommand));
            //AutocompleteText.OnFocusChanged += AutocompleteText_OnFocusChanged;
            SuggestionsListView = this.SearchList;
            SuggestionsListView.ItemSelected += SuggestionsListView_ItemSelected;
            SearchEntry.TextChanged += SearchEntry_TextChanged;
            SearchEntry.Unfocused += SearchEntry_Unfocused;
            SearchEntry.Focused += SearchEntry_Focused;
            //ClearIconImage.Command = new Command(OnClear);
            
        }

        private void SuggestionsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (this.ItemSelected != null)
            {
                this.ItemSelected(sender, e);
            }
        }

        void OnClear()
        {
            SearchEntry.Text = "";
        }
    }
}