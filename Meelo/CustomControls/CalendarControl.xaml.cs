﻿using GalaSoft.MvvmLight.Command;
using Meelo.Helper;
using Meelo.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GalaSoft.MvvmLight;
namespace Meelo.CustomControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CalendarControl : ContentView
	{
        MapPageViewModel viewModel;
        CalendarControlViewModel VM;
        
        
        public CalendarControl ()
		{
            InitializeComponent();
            
            VM = new CalendarControlViewModel();

            BindingContext = VM;
        }
        #region Calendar Functions
        public void CalendarInitilize()
        {
            viewModel = App.Locator.MapPageViewModelInstance;
            VM.CalendarCurrentMonth = String.Format("{0:MMMM}", DateTime.Now);
            VM.CalendarCurrentYear = String.Format("{0:yyyy}", DateTime.Now);
            VM.OnCalendarcancel = new RelayCommand(() =>
            {
                ResetCalendarLabel();
                ResetCalendarTexColor();
            });
            VM.OnCalendarApply = new RelayCommand(() =>
            {
                ResetCalendarTexColor();
                viewModel.isVisibleCalendar = false;
            });
            


            _MonthCount = DateTime.Now.Month;
            _YearCount = DateTime.Now.Year;
            VM.OnNextTimeCalendarClick = new RelayCommand(async () =>
            {
                await Task.Run(() =>
                {
                });
                await Task.Delay(100);
                VM.CalendarCurrentYear = (VM.CalendarCurrentMonth == "December" ? Convert.ToString(++_YearCount)
                : Convert.ToString(_YearCount));
                _MonthCount = (_MonthCount == 12 ? 1 : ++_MonthCount);
                VM.CalendarCurrentMonth = new DateTime(_YearCount, _MonthCount, 1)
                .ToString("MMMM", CultureInfo.InvariantCulture);
                ClearCalendar();
                CreateCalendar();

            });
            VM.OnPreTimeCalendarClick = new RelayCommand(async () =>
            {
                await Task.Run(() =>
                {

                });
                await Task.Delay(100);
                VM.CalendarCurrentYear = (VM.CalendarCurrentMonth == "January" ? Convert.ToString(--_YearCount)
                : Convert.ToString(_YearCount));
                _MonthCount = (_MonthCount == 1 ? 12 : --_MonthCount);
                VM.CalendarCurrentMonth = new DateTime(_YearCount, _MonthCount, 1)
                .ToString("MMMM", CultureInfo.InvariantCulture);
                ClearCalendar();
                CreateCalendar();
            });
            ResetCalendarLabel();
            CreateCalendar();
        }
        public DateTime CalendarSelectedDate { get; set; }
        public static int _MonthCount { get; set; }
        public static int _YearCount { get; set; }
        

        
        public int DayNumber(string dayName)
        {
            List<string> daysname = new List<string>();
            daysname.Add("MON");
            daysname.Add("TUE");
            daysname.Add("WED");
            daysname.Add("THU");
            daysname.Add("FRI");
            daysname.Add("SAT");
            daysname.Add("SUN");
            string day = daysname.Find(x => x == dayName.ToUpper());
            return daysname.IndexOf(day) + 1;
        }
        public void ClearCalendar()
        {
            ConvertWordToNumber convert = new ConvertWordToNumber();
            for (int i = 1; i <= 42; i++)
            {
                firstStack.FindByName<Label>(convert.ConvertNumbertoWords(i)).Text = "";
                firstStack.FindByName<Label>(convert.ConvertNumbertoWords(i)).TextColor = Color.Black;
            }
        }
        public void ResetCalendarLabel()
        {
            _MonthCount = DateTime.Now.Month;
            _YearCount = DateTime.Now.Year;

            string Selectedday = new DateTime(_YearCount, _MonthCount, DateTime.Now.Day)
          .ToString("ddd", CultureInfo.InvariantCulture);
            viewModel.CalendarLabSelecteddate = Selectedday + ", " + VM.CalendarCurrentMonth + " " + DateTime.Now.Day.ToString();
            viewModel.WorkspaceSearchDate = DateTime.Now;
            viewModel.isVisibleCalendar = false;

        }
        public void ResetCalendarTexColor()
        {
            ConvertWordToNumber convert = new ConvertWordToNumber();
            for (int i = 1; i <= 42; i++)
            {
                firstStack.FindByName<Label>(convert.ConvertNumbertoWords(i)).TextColor = Color.Black;
            }
        }
        public void CreateCalendar()
        {
            long fromDay = 0;

            ConvertWordToNumber convert = new ConvertWordToNumber();
            int daysInMonth = DateTime.DaysInMonth(_YearCount, _MonthCount);

            string dayName = new DateTime(_YearCount, _MonthCount, 1)
              .ToString("ddd", CultureInfo.InvariantCulture);
            long dayNumber = DayNumber(dayName);
            fromDay = dayNumber;
            string labelName = convert.ConvertNumbertoWords(dayNumber);
            firstStack.FindByName<Label>(labelName).Text = "1";
            dayNumber = dayNumber + 1;
            int datecount = 2;
            long lblName = dayNumber;

            for (; datecount <= daysInMonth; dayNumber++)
            {
                firstStack.FindByName<Label>(convert.ConvertNumbertoWords(lblName)).Text = datecount.ToString();
                var tabbLabel = new TapGestureRecognizer();
                tabbLabel.Tapped += (s, e) =>
                {
                    ResetCalendarTexColor();
                    var lab = s as Label;
                    lab.TextColor = Color.Red;
                    string Selectedday = new DateTime(_YearCount, _MonthCount, int.Parse(lab.Text))
               .ToString("ddd", CultureInfo.InvariantCulture);
                    viewModel.CalendarLabSelecteddate = Selectedday + ", " + VM.CalendarCurrentMonth + " " + lab.Text;
                    viewModel.WorkspaceSearchDate = new DateTime(_YearCount, _MonthCount, int.Parse(lab.Text));
                };
                tabbLabel.NumberOfTapsRequired = 1;
                firstStack.FindByName<Label>(convert.ConvertNumbertoWords(lblName)).GestureRecognizers.Add(tabbLabel);
                datecount++;
                lblName++;
            }
        }
        #endregion Calendar Functions

    }

    public class CalendarControlViewModel:ViewModelBase
    {
        public ICommand OnNextTimeCalendarClick { get; set; }
        public ICommand OnPreTimeCalendarClick { get; set; }
        
        public ICommand OnCalendarcancel { get; set; }
        public ICommand OnCalendarApply { get; set; }
        
        
        private string _CalendarCurrentYear;
        public string CalendarCurrentYear
        {
            get { return _CalendarCurrentYear; }
            set
            {
                _CalendarCurrentYear = value;
                RaisePropertyChanged("CalendarCurrentYear");
            }
        }
        private string _CalendarCurrentMonth;
        public string CalendarCurrentMonth
        {
            get { return _CalendarCurrentMonth; }
            set
            {
                _CalendarCurrentMonth = value;
                RaisePropertyChanged("CalendarCurrentMonth");
            }
        }
    }
}