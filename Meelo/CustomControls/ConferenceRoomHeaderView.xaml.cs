﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.CustomControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConferenceRoomHeaderView : StackLayout
	{
        public static readonly BindableProperty CommandProperty =
        BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ConferenceRoomHeaderView), null);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        public static void Execute(ICommand command)
        {
            if (command == null) return;
            if (command.CanExecute(null))
            {
                command.Execute(null);
            }
        }
        public static readonly BindableProperty ConfRoomsLabelProperty = BindableProperty.Create(nameof(ConfRoomsLabel), typeof(string), typeof(ConferenceRoomHeaderView), default(string), Xamarin.Forms.BindingMode.OneWay, propertyChanged: ConfRoomsLabelPropertyChanged);
        public string ConfRoomsLabel
        {
            get{return (string)GetValue(ConfRoomsLabelProperty);}

            set{SetValue(ConfRoomsLabelProperty, value);}
        }
        public static readonly BindableProperty ConfRoomAvailableCountLabelProperty = BindableProperty.Create(nameof(ConfRoomAvailableCountLabel), typeof(string), typeof(ConferenceRoomHeaderView), default(string), Xamarin.Forms.BindingMode.OneWay);
        public string ConfRoomAvailableCountLabel
        {
            get{return (string)GetValue(ConfRoomAvailableCountLabelProperty);}

            set{SetValue(ConfRoomAvailableCountLabelProperty, value); }
        }
        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty DropdownIconSourceProperty = BindableProperty.Create(nameof(DropdownIconSource), typeof(ImageSource), typeof(ConferenceRoomHeaderView), null);
        public ImageSource DropdownIconSource
        {
            get { return (ImageSource)GetValue(DropdownIconSourceProperty); }
            set { SetValue(DropdownIconSourceProperty, value); }
        }
        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty ListIconSourceProperty = BindableProperty.Create(nameof(ListIconSource), typeof(ImageSource), typeof(ConferenceRoomHeaderView), null);
        public ImageSource ListIconSource
        {
            get { return (ImageSource)GetValue(ListIconSourceProperty); }
            set { SetValue(ListIconSourceProperty, value); }
        }
        [TypeConverter(typeof(ImageSourceConverter))]
        public static readonly BindableProperty MapIconSourceProperty = BindableProperty.Create(nameof(MapIconSource), typeof(ImageSource), typeof(ConferenceRoomHeaderView), null);
        public ImageSource MapIconSource
        {
            get { return (ImageSource)GetValue(MapIconSourceProperty); }
            set { SetValue(MapIconSourceProperty, value); }
        }
        
        public static readonly BindableProperty ListIconIsVisibleProperty = BindableProperty.Create(nameof(ListIconIsVisible), typeof(bool), typeof(ConferenceRoomHeaderView), false, propertyChanged: ListIconIsVisiblePropertyChanged);
        public bool ListIconIsVisible
        {
            get { return (bool)GetValue(ListIconIsVisibleProperty); }
            set { SetValue(ListIconIsVisibleProperty, value); }
        }
        private static void ListIconIsVisiblePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (ConferenceRoomHeaderView)bindable;
            if (Convert.ToBoolean(newValue))
            {
                control.iconListView.IsVisible = true;
                control.iconMapView.IsVisible = false;
            }
            else
            {
                control.iconMapView.IsVisible = true;
                control.iconListView.IsVisible = false;
            }
        }
        private static void ConfRoomsLabelPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (ConferenceRoomHeaderView)bindable;
            control.lblConfRooms.Text = newValue.ToString();
        }
        private static void ConfRoomAvailableCountLabelPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var control = (ConferenceRoomHeaderView)bindable;
            control.lblConfRoomAvailableCount.Text = newValue.ToString();
        }

        public ConferenceRoomHeaderView ()
		{
			InitializeComponent ();
            lblConfRooms.SetBinding(Label.TextProperty, new Binding(path: "ConfRoomsLabel", source: this));
            lblConfRoomAvailableCount.SetBinding(Label.TextProperty, new Binding(path: "ConfRoomAvailableCountLabel", source: this));
            imgConfRoomsDropDown.SetBinding(Image.SourceProperty, new Binding(path: "DropdownIconSource", source: this));
            iconMapView.SetBinding(Image.SourceProperty, new Binding(path: "MapIconSource", source: this));
            iconListView.SetBinding(ImageButton.SourceProperty, new Binding(path: "ListIconSource", source: this));
            //iconListView.SetBinding(ImageButton.IsVisibleProperty, new Binding(path: "ListIconIsVisible", source: this));
            iconListView.Command = new Command(_ => Execute(Command));
            iconMapView.Command = new Command(_ => Execute(Command));

        }
        #region Functions

        #endregion Functions
    }
}