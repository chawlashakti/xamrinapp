﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    [DataContract]
    public class CalendarEvent
    {

        [DataMember(Name = "Subject")]
        public string Subject { get; set; }

        [DataMember(Name = "Start")]
        public string Start { get; set; }

        [DataMember(Name = "End")]
        public string End { get; set; }

        [DataMember(Name = "Body")]
        public CalendarBody Body { get; set; }

        [DataMember(Name = "Attendees")]
        public CalendarAttendee[] Attendees { get; set; }
    }
}
