﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class AzureSearchModel
    {
        public int BuildingId { get; set; }

        public bool IsCurrentLocationIconVisble { get; set; }

        [JsonProperty("@search.text")]
        public string SearchText { get; set; }
    }

    public class SearchResult
    {
        public List<AzureSearchModel> value { get; set; }
    }
    public class AzureSearchResponseModel
    {
        public string WorkspaceBldId { get; set; }

        public bool IsCurrentLocationIconVisble { get; set; }

        [JsonProperty("@search.text")]
        public string SearchText { get; set; }
    }
    public class AzureSearchResult
    {
        public List<AzureSearchResponseModel> value { get; set; }
    }
}
