﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class ServiceInfo
    {
        public string AccessToken { get; set; }
        public string ResourceId { get; set; }
        public string ApiEndpoint { get; set; }
    }
}
