﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meelo.Models
{
    public class PatterenedRecurrrence
    {
        public RecurrencePattern RecurrencePattern { get; set; }

        public RecurrenceRange RecurrenceRange { get; set; }
    }

    public class RecurrencePattern
    {

        public int DayOfMonth { get; set; }
        public IEnumerable<string> DaysOfWeek { get; set; }
        public string FirstDayOfWeek { get; set; }
        public string Index { get; set; }
        public int Interval { get; set; }
        public int Month { get; set; }
        public string Type { get; set; }
    }
    public class RecurrenceRange
    {
        public DateTimeOffset EndDate { get; set; }
        public int NumberOfOccurrences { get; set; }
        public string RecurrenceTimeZone { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public string Type { get; set; }
    }
}
