﻿using Meelo.Models;
using System;
using System.Threading.Tasks;

namespace Meelo.Webservices
{
    public interface IMeetingCheckInCheckOut
    {
        Task<OutputBaseClass> CheckInMeeting(Event checkInEvent);

        Task<OutputBaseClass> CheckOutMeeting(Event checkOutEvent);

        Task<OutputBaseClass> RateWorkspace(WorkspaceReviewInput ratingParameters);
    }
}
