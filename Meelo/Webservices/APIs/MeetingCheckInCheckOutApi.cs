﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using Meelo.Models;

namespace Meelo.Webservices
{
    public class MeetingCheckInCheckOutApi : IMeetingCheckInCheckOut
    {
        public async Task<OutputBaseClass> CheckInMeeting(Event checkInEvent)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;

            OutputBaseClass outputData = new OutputBaseClass();

            try
            {
                string restURL =Helper.Constants.ApiBaseUrl + "/Workspace/CheckIn";
                var json = JsonConvert.SerializeObject(checkInEvent);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage result = client.PostAsync(restURL, httpContent).Result;

                if (result.IsSuccessStatusCode)
                {
                    var contentResponse = await result.Content.ReadAsStringAsync();
                    outputData = JsonConvert.DeserializeObject<OutputBaseClass>(contentResponse);
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                client.Dispose();
            }
            return outputData;

        }
        public async Task<OutputBaseClass> CheckOutMeeting(Event checkOutEvent)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;

            OutputBaseClass outputData = new OutputBaseClass();

            try
            {
                string restURL = Helper.Constants.ApiBaseUrl + "/Workspace/CheckOut";
                var json = JsonConvert.SerializeObject(checkOutEvent);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage result = client.PostAsync(restURL, httpContent).Result;

                if (result.IsSuccessStatusCode)
                {
                    var contentResponse = await result.Content.ReadAsStringAsync();
                    outputData = JsonConvert.DeserializeObject<OutputBaseClass>(contentResponse);
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
                client.Dispose();
            }
            return outputData;

        }

        public async Task<OutputBaseClass> RateWorkspace(WorkspaceReviewInput ratingParameters)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 999999;

            OutputBaseClass outputData = new OutputBaseClass();

            try
            {
                string restURL = Helper.Constants.ApiBaseUrl + "/rate/workspace";
                var json = JsonConvert.SerializeObject(ratingParameters);
                HttpContent httpContent = new StringContent(json);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                HttpResponseMessage result = client.PostAsync(restURL, httpContent).Result;

                if (result.IsSuccessStatusCode)
                {
                    var contentResponse = await result.Content.ReadAsStringAsync();
                    outputData = JsonConvert.DeserializeObject<OutputBaseClass>(contentResponse);
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
                client.Dispose();
            }
            return outputData;

        }
    }
}
