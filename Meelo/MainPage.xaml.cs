﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Meelo
{
	public partial class MainPage : TabbedPage
    {
        
        public MainPage()
		{
            
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
        }
        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();

            Title = CurrentPage?.Title;
            string Page = CurrentPage?.ClassId;
            
            switch (Title)
            {
                case "Search":
                    {
                        //App.Locator.MapPageViewModelInstance.AddPinsOnMap(App.position, true);
                        App.Locator.MapPageViewModelInstance.TimeSlot(DateTime.Now);
                        App.Locator.MapPageViewModelInstance.GetAvailableWorkspaces();
                        App.Locator.MapPageViewModelInstance.CalculateStartAndEndTime();
                    }
                    break;
                case "Meetings":
                    App.Locator.MeetingPageViewModelInstance.LoadMeeting();
                    break;
                case "Apps":
                    break;
                case "More":
                    break;
                    //case "List":
                    //    App.Locator.ListViewPageViewModelInstance.LoadListViewPage();
                    //    break;
            }
        }
        
    }
}
