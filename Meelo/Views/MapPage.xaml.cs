﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Meelo.CustomControls;
using Meelo.Models;
using Meelo.ViewModels;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Meelo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
        MapPageViewModel viewModel;
        private readonly IGeolocator locator = CrossGeolocator.Current;
        public MapPage ()
		{
            
            
            IsBusy = true;
            InitializeComponent ();
            viewModel = App.Locator.MapPageViewModelInstance;
            GetGPSCordinates();
            viewModel.meeloMapObj = MeeloMap;
            viewModel.RangeSlider = this.RangeSlider;
            BindingContext = viewModel;
            Calendar.CalendarInitilize();
            RangeSlider.LeftValueChanged += RangeSlider_LeftValueChanged;
            RangeSlider.RightValueChanged += RangeSlider_RightValueChanged;
            RangeSlider.ValueChangeCompleted += RangeSlider_ValueChangeCompleted;
            viewModel.IsBusy = false;
            viewModel.IsListIconVisible = true;
            SearchControl.OnFocusChanged += SearchControl_OnFocusChanged;
            SearchControl.ItemSelected += SearchControl_ItemSelected;
            viewModel.WorkspaceListContainer = this.WorkspaceList;
            CalculateStartandEndTime();
        }
        public static async Task<Position> GetCurrentPosition()
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;

                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    //got a cahched position, so let's use it.
                    return position;
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    return null;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);

            }
            catch (Exception ex)
            {
                //Debug.WriteLine("Unable to get location: " + ex);
            }

            if (position == null)
                return null;

            //var output = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
            //        position.Timestamp, position.Latitude, position.Longitude,
            //        position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);

            //Debug.WriteLine(output);

            return position;
        }
        public async void GetGPSCordinates()
        {
            viewModel.MyPosition =await GetCurrentPosition();
            //App.position = new Position {Latitude= 18.5741873,Longitude= 73.8793412 }; 
            //await locator.GetPositionAsync(TimeSpan.FromSeconds(20));
        }
        private void SearchControl_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            AzureSearchResponseModel SearchObj =(AzureSearchResponseModel) e.SelectedItem ;
            if (SearchObj != null)
            {
              viewModel.IsCurrentLocation = false;
                string[] Ids = SearchObj.WorkspaceBldId.Split('-');
                SearchControl.SearchText = SearchObj.SearchText;
                viewModel.GetAvailableWorkspaces(Convert.ToInt64(Ids[0]),SearchObj.SearchText);
                viewModel.VisibilitySetting(Helper.Constants.PageView.List);
                if (viewModel.WorkspaceRecord.Count == 0)
                {
                    viewModel.IsListNotEmpty = false;
                    viewModel.IsEmptyMsgVisible = true;
                }
                else
                {
                    viewModel.IsListNotEmpty = true;
                    viewModel.IsEmptyMsgVisible = false;
                    viewModel.CreateListView();
                }
               
            }
        }

        private void SearchControl_OnFocusChanged(object sender, EventArgs e)
        {
         if((e as FocusEventArgs).IsFocused)
            {
                if(viewModel.CurrentPage!= Helper.Constants.PageView.Search)
                viewModel.VisibilitySetting(Helper.Constants.PageView.Search);
            }
         //else
         //   {
         //       ConfRoomHeader.IsVisible = true;
         //       SearchFilters.IsVisible = true;
         //       StackTimeRangeslider.IsVisible = true;
         //       mapView.IsVisible = true;
         //       viewModel.SearchHeaderVisible = false;
         //   }
           
        }
        #region Events
        private void DatePicker_Unfocused(object sender, FocusEventArgs e)
        {
        }
        private async void RangeSlider_ValueChangeCompleted(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                viewModel.IsBusy = true;
            });
            await Task.Delay(200);
            viewModel.GetAvailableWorkspaces();
            //viewModel.ResetTimeRangeValues();
            //viewModel.LoadPage();
            await Task.Run(() =>
            {
                viewModel.IsBusy = false;
            });
        }

        private void RangeSlider_RightValueChanged(object sender, EventArgs<double> e)
        {
            CalculateStartandEndTime();

        }

        private void RangeSlider_LeftValueChanged(object sender, EventArgs<double> e)
        {
            CalculateStartandEndTime();
        }

        public void CalculateStartandEndTime()
        {
            var LMins = RangeSlider.LeftValue % 1;
            var UMins = RangeSlider.RightValue % 1;
            string StartAmPm = "am";
            string EndAmPm = "am";
            int LeftvalueTemp = 0;
            int RightvalueTemp = 0;
            LeftvalueTemp = Convert.ToInt32(RangeSlider.LeftValue - LMins);
            RightvalueTemp = Convert.ToInt32(RangeSlider.RightValue - UMins);

            LMins = LMins * 0.6f;

            UMins = UMins * 0.6f;

            if (RangeSlider.StartTime.AddHours(LeftvalueTemp).Hour > 11.0)
                StartAmPm = "pm";
            else
                StartAmPm = "am";
            if (RangeSlider.StartTime.AddHours(RightvalueTemp).Hour > 11.0)
                EndAmPm = "pm";
            else
                EndAmPm = "am";
            viewModel.MeetingTime = (((RangeSlider.StartTime.AddHours(LeftvalueTemp).Hour + 11) % 12) + 1).ToString("00") + ":" + (LMins * 100).ToString("00")
                + StartAmPm.ToString() + " - " + (((RangeSlider.StartTime.AddHours(RightvalueTemp).Hour + 11) % 12) + 1).ToString("00") + ":" + (UMins * 100).ToString("00") + " " + EndAmPm.ToString();
            viewModel.WorkspaceStartTime = RangeSlider.StartTime.AddHours(LeftvalueTemp).TimeOfDay;
            viewModel.WorkspaceEndTime = RangeSlider.StartTime.AddHours(RightvalueTemp).TimeOfDay;
        }
        #endregion Events
    }
}