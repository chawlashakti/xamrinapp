﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;
using Meelo.CustomControls;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meelo.ViewModels
{
   public class ListViewPageViewModel: ViewModelBase
    {
        #region Members
        private readonly INavigationService _navigationService;
        private IDialogService _dialogService;
        #endregion Members
        #region Properties

        #endregion Properties
        #region Constructor
        public ListViewPageViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            if (navigationService == null)
                throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            if (dialogService == null)
                throw new ArgumentNullException("navigationService");
            _dialogService = dialogService;
        }
        #endregion Constructor
        #region Functions
        
        #endregion Functions
    }
}
