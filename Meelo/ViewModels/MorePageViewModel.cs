﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Meelo.Helper;
using Meelo.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Meelo.ViewModels
{
   public class MorePageViewModel: ViewModelBase
    {
        #region Members
        public ICommand OnLogoutClick { get; set; }
        private readonly INavigationService _navigationService;
        private IDialogService _dialogService;

        public ICommand OnContentClick { get; set; }
        
        #endregion Members
        
        #region Properties
        private bool _IsBusy;

        public bool IsBusy
        {
            get => _IsBusy;
            set
            {
                _IsBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }
        private string _userNameInitialLetter;
        public string UserNameInitialLetter
        {
            get { return _userNameInitialLetter; }
            set
            {
                _userNameInitialLetter = value;
                RaisePropertyChanged("UserNameInitialLetter");
            }
        }
        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                RaisePropertyChanged("UserName");
            }
        }
        private string _userEmailId;
        public string UserEmailId
        {
            get { return _userEmailId; }
            set
            {
                _userEmailId = value;
                RaisePropertyChanged("UserEmailId");
            }
        }
        #endregion Properties

        #region Constructor
        public MorePageViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            IsBusy = false;

            if (navigationService == null)
                throw new ArgumentNullException("navigationService");

            _navigationService = navigationService;

            if (dialogService == null)
                throw new ArgumentNullException("navigationService");

            _dialogService = dialogService;

            OnLogoutClick = new Command(Logout);
            OnContentClick = new  Command(ContentSharing);

            GetUserNameInitials();
        }
        #endregion Constructor


        #region Functions
       async void Logout()
        {
            try
            {
                await Task.Run(() =>
                {
                    IsBusy = true;
                });
                await Task.Delay(200);

                //App.isLogout = true;
                //App.isShowLoginoptions = true;


                 // Shakti - should this be called ?
               // AuthenticationHelper.SignOut();

                foreach (var user in App.PCA.Users)
                {
                    App.PCA.Remove(user);
                    
                    // TokenForUser should be set to NULL ?
                }
                //DependencyService.Get<IAccessTokenFile>().DeleteData("LoginDetails");

                _navigationService.NavigateTo(Constants.LoginOptions);

                //IsMoreOptionsDataVisible = false;
                //App.universalToken = null;

                await Task.Run(() =>
                {
                    IsBusy = false;
                });
            }
            catch (Exception ex)
            {
                // Exception logging out
            }
        }

        async void ContentSharing()
        {
            // content to one drive here but need more data to connect to 

            var user = App.PCA.Users.First();
            //AuthenticationHelper.TokenForUser;


            var uri = "https://graph.microsoft.com/v1.0/me/drive/root/children";

            // client to connect to and get the resonse ?
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthenticationHelper.TokenForUser);
            var response = await httpClient.GetAsync(uri);
            var jsonString = await response.Content.ReadAsStringAsync();

            // use this user information to connect to the one drive

        }

        public void GetUserNameInitials()
        {
            try
            {
                string user = App.UserName;
                if (!String.IsNullOrEmpty(user))
                {
                    string[] nameArray = user.Split(new char[0]);
                    if (nameArray.Length >= 2)
                    {
                        char firstNameLetter = nameArray[0][0];
                        char lastNameLetter = nameArray[1][0];
                        UserNameInitialLetter = firstNameLetter.ToString() + lastNameLetter.ToString();
                    }
                    else if (nameArray.Length == 1)
                    {
                        char firstNameLetter = nameArray[0][0];
                        UserNameInitialLetter = firstNameLetter.ToString();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion Functions
    }
}
