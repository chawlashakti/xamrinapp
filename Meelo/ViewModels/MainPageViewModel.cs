﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meelo.ViewModels
{
   
    public class MainPageViewModel: ViewModelBase
    {
        private readonly INavigationService _navigationService;
    private IDialogService _dialogService;
        public MainPageViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            if (navigationService == null)
                throw new ArgumentNullException("navigationService");
            _navigationService = navigationService;

            if (dialogService == null)
                throw new ArgumentNullException("navigationService");
            _dialogService = dialogService;
        }
    }
}
